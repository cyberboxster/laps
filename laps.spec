%define debug_package %{nil}
Name:		laps
Version:	0.0.5
Release:	1
Summary:	local administrator password solution

Group:		system
License:	CC-BY-SA 4.0
URL:		https://bgstack15.wordpress.com/
Source0:	laps-%{version}.tgz

BuildArch:	noarch
BuildRequires:	coreutils
Requires:	bgscripts-core >= 1.4.4
Requires:	krb5-workstation
Requires:	openldap-clients
Requires:	passwd
Requires:	util-linux
Requires:	libpwquality
Requires:	findutils
Requires:	coreutils
Requires:	sed
Requires:	gawk

%description
laps provides the GNU/Linux client for the LAPS (Local Administrator Password Solution).

%prep
%setup -q -c %{name}

%build
#%%configure
#make %%{?_smp_mflags}

%install
#make install DESTDIR=%{buildroot}
# the %%name* allows for either laps-0.0.0/ or laps/ dirname.
cp -pr %{name}*/src/* "%{buildroot}"

%files
%doc %{_docdir}/%{name}/*
%config %attr(644, -, -) %{_sysconfdir}/%{name}/*
%config %attr(600, -, -) %{_sysconfdir}/cron.d/*.cron
%{_datadir}/%{name}

%changelog
* Thu Jul 18 2019 B Stack <bgstack15@gmail.com> 0.0.5-1
- rpm built
