#!/bin/sh
# Filename: laps.sh
# Location: /usr/share/
# Author: bgstack15@gmail.com
# Startdate: 2018-10-17 11:38:00
# Title: Local Administrator Password Solution for Linux
# Purpose: LAPS Equivalent for GNU/Linux
# Package: laps
# History: see upstream project at https://gitlab.com/bgstack15/laps
# Usage:
# Reference: ftemplate.sh 2018-09-12a; framework.sh 2018-09-12a
# Improve:
# Dependencies:
#    bundled: dependencies/datetime.py
#    framework.sh, kinit-host, shldap >= 20190717 (bgscripts-core >= 1.4.4)
#    kinit, klist (krb5-workstation)
#    ldapsearch, ldapmodify (openldap-clients)
#    passwd (passwd)
#    logger (util-linux)
#    pwmake (libpwquality)
#    xargs (find-utils)
#    cp (coreutils)
#    sed (sed)
#    awk (gawk)
fiversion="2018-09-12a"
lapsversion="2019-07-17a"

usage() {
   ${PAGER:-/usr/bin/less -F} >&2 <<ENDUSAGE
laps is the Local Administrator Password Solution for GNU/Linux.
usage: laps.sh [-duV] [-c conffile] [-t|-a] [-f] [-r [-u <username>] [-h <hostname>]] [-i]
version ${lapsversion}
 -d debug   Show debugging info, including parsed variables.
 --usage    Show this usage block.
 -V version Show script version number.
 -c conf    Read in this config file. Default is /etc/laps/laps.conf
 -f force   Skip the time check and just update the password regardless.
 --noforce  Do not force. Overrides environment variable LAPS_FORCE.
 -t test    Dry run only. Useful with debugging on.
 -a apply   Turn off dry run. Default.
 -r read    Read password; do not set it. Can only be used by a domain admin. Can only be used with -u.
 -u user    Connect with kerberos ticket for this user. Default is "machine" to use host keytab.
 -h <hostname>  Read this hostname instead of \$( hostname -s )
 -i interactive Allow laps to prompt for password if a keytab does not yet exist.
Debug levels:
 0 Silent
 9 displays sensitive info, specifically the generated password
10 function trace
Environment variables:
 See documentation at /usr/share/doc/laps/ for full explanation.
Return values:
 0 Normal
 1 Help or version info displayed
 2 Parameters or environment variables are invalid
 3 Incorrect OS type
 4 Unable to find dependency
 5 Not run as root or sudo
 6 Unable to get kerberos ticket
 7 Unable to set ldap attributes
 8 Unable to change password
ENDUSAGE
}

# DEFINE FUNCTIONS

debuglevoutput() {
   # call: commandthatgeneratesstdout | debuglevoutput 8
   # output: output to standard error prepended with "debug8: " the contents of the pipe
   ___dlo_threshold="${1}"
   ___dlo_silent="${2}"
   if debuglev "${___dlo_threshold}" ;
   then
      if test -n "${___dlo_silent}" ;
      then
         cat 1>&2
      else
         sed -r -e "s/^/debug${___dlo_threshold}: /;" 1>&2
      fi
   else
      cat 1>/dev/null 2>&1
   fi
}

read_workflow() {

   # 1. get user kerberos ticket
   get_user_kerberos_ticket "${LAPS_KERBEROS_USER}" "${LAPS_USER_IS_ROOT}" "${LAPS_KRB5CC_TMPFILE}" "${LAPS_INTERACTIVE}" "${LAPS_KINIT_BIN}" "${LAPS_KLIST_BIN}"

   # 2. fetch and display host password
   get_attrib_from_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_ATTRIB_PW}" "${LAPS_LDAPCONF}" "${LAPS_KRB5CC_TMPFILE}" "${LAPS_LDAPSEARCH_STATUS_TMPFILE}"

   # 3. fetch and display expiration if the various debug levels
   # this is called for the debuglev actions inside it, not for the output directly
   wrapper_get_timestamp_from_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_ATTRIB_TIME}" "${LAPS_LDAPCONF}" "${LAPS_DATETIME_PY}" "${LAPS_KRB5CC_TMPFILE}" "${LAPS_LDAPSEARCH_STATUS_TMPFILE}" 1>/dev/null

}

main_workflow() {

   # 0. fail if not root
   if test "${LAPS_USER_IS_ROOT}" = "0" ;
   then
      ferror "${scriptfile}: 5. To set password, run this command as root. Aborted."
      exit 5
   fi

   # 1. kinit-host
   get_host_keytab "${LAPS_KINIT_HOST_SCRIPT}" "${LAPS_KINIT_HOST_SCRIPT_OPTS}" "${LAPS_KLIST_BIN}" "${LAPS_KRB5CC_TMPFILE}" || { ferror "${0}: unable to get host kerberos ticket. Aborted." ; exit 6 ; }

   # 2. fetch timestamp from ldap
   LAPS_epoch="$( wrapper_get_timestamp_from_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_ATTRIB_TIME}" "${LAPS_LDAPCONF}" "${LAPS_DATETIME_PY}" "${LAPS_KRB5CC_TMPFILE}" "${LAPS_LDAPSEARCH_STATUS_TMPFILE}" )"
   LAPS_epoch_response="$?"
   test ${LAPS_epoch_response} -eq 0 || return "${LAPS_epoch_response}"

   # 3. check timestamp to see if close to expiration
   check_ts_against_expiration_threshold "${LAPS_THRESHOLD}" "${LAPS_epoch}" "${LAPS_FORCE}"

   # 4. generate new password and timestamp
   LAPS_phrase="$( wrapper_genpw "${LAPS_PWGEN_SCRIPT}" "${LAPS_PWGEN_FLAGS}" )"
   LAPS_timestamp="$( get_current_filetime "${LAPS_DATETIME_PY}" "${LAPS_TIMELIMIT}" )"

   # 5. update ldap
   update_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_LDAPSEARCH_UNIQUE_ID}" "${LAPS_LDAPCONF}" "${LAPS_KRB5CC_TMPFILE}" ":" "${LAPS_ATTRIB_PW}:${LAPS_ATTRIB_TIME}" "${LAPS_phrase}:${LAPS_timestamp}" "${LAPS_LDIF_TMPFILE}" "${LAPS_LDAPMODIFY_BIN}" "${LAPS_LDAPMODIFY_FLAGS}" "${LAPS_TEST}" "${LAPS_LDAPSEARCH_STATUS_TMPFILE}"

   # 6. if ^ was successful, change password for configured user
   wrapper_change_password "${LAPS_phrase}" "${LAPS_USER}" "${LAPS_PASSWD_BIN}" "${LAPS_TEST}"

   # 7. tell syslog password was updated
   wrapper_log "${LAPS_LOG_BIN}" "${LAPS_LOG_FLAGS}" "${LAPS_LOG_MSG}" "${LAPS_TEST}"

}

wrapper_get_timestamp_from_ldap() {
   # call: wrapper_get_timestamp_from_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_ATTRIB_TIME}" "${LAPS_LDAPCONF}" "${LAPS_DATETIME_PY}" "${LAPS_KRB5CC_TMPFILE}" "${LAPS_LDAPSEARCH_STATUS_TMPFILE}"
   debuglev 10 && ferror "wrapper_get_timestamp_from_ldap $@"
   ___wgtfl_ldapsearch_bin="${1}"
   ___wgtfl_ldapsearch_flags="${2}"
   ___wgtfl_ldapsearch_filter="${3}"
   ___wgtfl_attrib="${4}"
   ___wgtfl_ldapconf="${5}"
   ___wgtfl_datetime_py="${6}"
   ___wgtfl_krb5cc_tmpfile="${7}"
   ___wgtfl_ldapsearch_status_tmpfile="${8}"

   ts_filetime="$( get_attrib_from_ldap "${___wgtfl_ldapsearch_bin}" "${___wgtfl_ldapsearch_flags}" "${___wgtfl_ldapsearch_filter}" "${___wgtfl_attrib}" "${___wgtfl_ldapconf}" "${___wgtfl_krb5cc_tmpfile}" "${___wgtfl_ldapsearch_status_tmpfile}" )"
   ts_filetime_response="$?"
   test ${ts_filetime_response} -eq 0 || return "${ts_filetime_response}"

   ts_epoch=0
   if test -n "${ts_filetime}" ;
   then
      debuglev 3 && ferror "timestamp(FILETIME): ${ts_filetime}"
      ts_epoch="$( "${___wgtfl_datetime_py}" -e "${ts_filetime}" )"
   fi
   debuglev 2 && ferror "timestamp(epoch): ${ts_epoch}"
   debuglev 1 && ferror "timestamp(UTC): $( date -u -d "@${ts_epoch}" "+%FT%TZ" )"

   echo "${ts_epoch}"
}

check_ts_against_expiration_threshold() {
   # call: check_ts_against_expiration_threshold "${LAPS_THRESHOLD}" "${LAPS_epoch}" "${LAPS_FORCE}"
   debuglev 10 && ferror "check_ts_against_expiration_threshold $@"
   ___ctaeh_threshold="${1}"
   ___ctaeh_epoch="${2}"
   ___ctaeh_force="${3}"

   ___ctaeh_thres="$( date -u -d "now+${___ctaeh_threshold}" "+%s" )"

   # if flag --force was used, just skip the check
   if fistruthy "${___ctaeh_force}"
   then
      return
   fi

   if ! fisnum "${___ctaeh_thres}" || ! fisnum "${___ctaeh_epoch}" ;
   then
      ferror "${scriptfile}: 4 fatal! cannot compare ${___ctaeh_thres} to ${___ctaeh_epoch}. Aborted."
      exit 4
   fi

   if ! test ${___ctaeh_thres} -ge ${___ctaeh_epoch} ;
   then
      debuglev 1 && echo "${scriptfile}: No changes required."
      exit 0
   fi
}

genpw() {
   # call: genpw "${LAPS_PWGEN_SCRIPT}" "${LAPS_PWGEN_FLAGS}"
   debuglev 10 && ferror "genpw $@"
   ___genpw_pwgen_script="${1}"
   ___genpw_pwgen_flags="${2}"

   "${___genpw_pwgen_script}" ${___genpw_pwgen_flags}
}

wrapper_genpw() {
   # call: wrapper_genpw "${LAPS_PWGEN_SCRIPT}" "${LAPS_PWGEN_FLAGS}"
   # output: full phrase on stdout
   debuglev 10 && ferror "wrapper_genpw $@"
   ___wg_pwgen_script="${1}"
   ___wg_pwgen_flags="${2}"

   ___wg_phrase="$( genpw "${___wg_pwgen_script}" "${___wg_pwgen_flags}" )"
   ___wg_masked="$( echo "${___wg_phrase}" | head -c 8 ; echo "${___wg_phrase}" | tail -c +9 | sed -r -e 's/./*/g;' )"
   #echo "___wg_phrase=\"${___wg_phrase}\""
   #echo "___wg_masked=\"${___wg_masked}\""
   if debuglev 2 ;
   then
      if fistruthy "${NO_MASK}"  ;
      then
         ferror "Using ${___wg_phrase}"
      else
         ferror "Using ${___wg_masked}"
      fi
   fi

   echo "${___wg_phrase}"

}

get_current_filetime() {
   # call: get_current_filetime "${LAPS_DATETIME_PY}" "${LAPS_TIMELIMIT}"
   # returns: FILETIME format of current timestamp on stdout
   debuglev 10 && ferror "get_current_filetime $@"
   ___gcf_datetime_py="${1}"
   ___gcf_timelimit="${2}"

   ___gcf_timestamp="$( "${___gcf_datetime_py}" -f "$( date -u -d "now+${___gcf_timelimit}" "+%s" )" )"

   if ! fisnum "${___gcf_timestamp}" ;
   then
      ferror "${scriptfile}: 4 fatal! Could not generate valid timestamp. Aborted."
      ferror "what was generated: \"${___gcf_timestamp}\""
      exit 4
   fi
   debuglev 3 && ferror "new timestamp(FILETIME): ${___gcf_timestamp}"

   echo "${___gcf_timestamp}"
}

wrapper_change_password() {
   # call: wrapper_change_password "${LAPS_phrase}" "${LAPS_USER}" "${LAPS_PASSWD_BIN}" "${LAPS_TEST}"
   debuglev 10 && ferror "wrapper_change_password $@"
   ___wcp_phrase="${1}"
   ___wcp_user="${2}"
   ___wcp_passwd_bin="${3}"
   ___wcp_test="${4}"

   if fistruthy "${___wcp_test}" ;
   then
      echo "0" > "${LAPS_PASSWORD_STATUS_TMPFILE}"
   else
      ___wcp_stdout="$( printf "%s\n%s\n" "${___wcp_phrase}" "${___wcp_phrase}" | "${___wcp_passwd_bin}" "${___wcp_user}" 2>&1 ; echo "$?" > "${LAPS_PASSWORD_STATUS_TMPFILE}" )"
   fi
   ___wcp_passwd_result="$( cat "${LAPS_PASSWORD_STATUS_TMPFILE}" )"

   case "${___wcp_passwd_result}" in
      0)
         # successful operation
         debuglev 4 && ferror "${___wcp_stdout}"
         ;;
      *)
         # failed operation
         ferror "${scriptfile}: 8 fatal! Unable to change password for ${___wcp_user}:\n${___wcp_stdout}"
         exit 8
         ;;
   esac
}

wrapper_log() {
   # call: wrapper_log "${LAPS_LOG_BIN}" "${LAPS_LOG_FLAGS}" "${LAPS_LOG_MSG}" "${LAPS_TEST}"
   debuglev 10 && ferror "wrapper_log $@"
   ___wl_log_bin="${1}"
   ___wl_log_flags="${2}"
   ___wl_log_msg="${3}"
   ___wl_test="${4}"

   if ! fistruthy "${___wl_test}" ;
   then
      "${___wl_log_bin}" ${___wl_log_flags} "${___wl_log_msg}"
   fi

}

# DEFINE TRAPS

clean_laps() {
   # use at end of entire script if you need to clean up tmpfiles
   # rm -f "${tmpfile1}" "${tmpfile2}" 2>/dev/null

   # Delayed cleanup
   if test -z "${LAPS_NO_CLEAN}" ;
   then
      nohup /bin/sh <<EOF 1>/dev/null 2>&1 &
sleep "${LAPS_CLEANUP_SEC:-3}" ; /bin/rm -r "${LAPS_TMPDIR:-NOTHINGTODELETE}" 1>/dev/null 2>&1 ;
EOF
   fi
}

CTRLC() {
   # use with: trap "CTRLC" 2
   # useful for controlling the ctrl+c keystroke
   :
}

CTRLZ() {
   # use with: trap "CTRLZ" 18
   # useful for controlling the ctrl+z keystroke
   :
}

parseFlag() {
   flag="$1"
   hasval=0
   case ${flag} in
      # INSERT FLAGS HERE
      "d" | "debug" | "DEBUG" | "dd" ) setdebug; ferror "debug level ${debug}" ; __debug_set_by_param=1;;
      "usage" | "help" ) usage; exit 1;;
      "V" | "fcheck" | "version" ) ferror "${scriptfile} version ${lapsversion}"; exit 1;;
      #"i" | "infile" | "inputfile" ) getval; infile1=${tempval};;
      "c" | "conf" | "conffile" | "config" ) getval; conffile="${tempval}";;
      "t" | "test" | "dryrun" | "dry-run" ) LAPS_TEST=1 ; LAPS_ACTION="update" ;;
      "a" | "apply" | "nodryrun" | "no-dryrun" | "no-dry-run" ) LAPS_TEST=0 ; LAPS_ACTION="update" ;;
      "f" | "force" ) LAPS_FORCE=1 ; LAPS_ACTION="update" ;;
      "noforce" | "notforce" | "not-force" | "no-force" ) LAPS_FORCE=0 ; LAPS_ACTION="update" ;;
      "u" | "user" ) getval; LAPS_KERBEROS_USER="${tempval}" ;;
      "r" | "read" | "readonly" | "read-only" ) LAPS_ACTION="read" ;;
      "i" | "interactive" ) LAPS_INTERACTIVE=1 ;;
      "ni" | "nointeractive" ) LAPS_INTERACTIVE=0 ;;
      "h" | "host" | "hostname" | "server" ) getval; LAPS_HOST="${tempval}" ;;
   esac

   debuglev 10 && { test ${hasval} -eq 1 && ferror "flag: ${flag} = ${tempval}" || ferror "flag: ${flag}"; }
}

# DETERMINE LOCATION OF FRAMEWORK
f_needed=20180912
while read flocation ; do if test -e ${flocation} ; then __thisfver="$( sh ${flocation} --fcheck 2>/dev/null )" ; if test ${__thisfver} -ge ${f_needed} ; then frameworkscript="${flocation}" ; break; else printf "Obsolete: %s %s\n" "${flocation}" "${__this_fver}" 1>&2 ; fi ; fi ; done <<EOFLOCATIONS
/usr/share/bgscripts/framework.sh
/usr/share/laps/dependencies/framework.sh
/usr/share/laps/framework.sh
EOFLOCATIONS
test -z "${frameworkscript}" && echo "$0: framework not found. Aborted." 1>&2 && exit 4

# INITIALIZE VARIABLES
# variables set in framework:
# today server thistty scriptdir scriptfile scripttrim
# is_cronjob stdin_piped stdout_piped stderr_piped sendsh sendopts
. ${frameworkscript} || echo "$0: framework did not run properly. Continuing..." 1>&2
infile1=
outfile1=
logfile=${scriptdir}/${scripttrim}.${today}.out
define_if_new interestedparties "bgstack15@gmail.com"
# SIMPLECONF
define_if_new default_conffile "/etc/laps/laps.conf"
#define_if_new defuser_conffile ~/.config/laps/laps.conf

# REACT TO OPERATING SYSTEM TYPE
case $( uname -s ) in
   Linux) : ;;
   FreeBSD) : ;;
   *) echo "${scriptfile}: 3. Indeterminate OS: $( uname -s )" 1>&2 && exit 3;;
esac

# REACT TO ROOT STATUS
LAPS_USER_IS_ROOT=0
case ${is_root} in
   1) # proper root
      LAPS_USER_IS_ROOT=1 ;;
   sudo) # sudo to root
      LAPS_USER_IS_ROOT=1 ;;
   "") # not root at all
      #ferror "${scriptfile}: 5. Please run as root or sudo. Aborted."
      #exit 5
      :
      ;;
esac

# LOAD SHLDAP
setval 1 LAPS_SHLDAP_discovered <<EOFSENDSH     # if $1="1" then setvalout="critical-fail" on failure
${LAPS_SHLDAP}
${SHLDAP}
/usr/share/bgscripts/shldap.sh
/usr/lib/bgscripts/shldap.sh
EOFSENDSH
test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. LAPS_SHLDAP not found. Aborted." && exit 4
LAPS_SHLDAP_MINIMUM=20190717
. "${LAPS_SHLDAP_discovered}"
if ! test $( echo "${SHLDAP_VERSION}" | tr -dc '[[:digit:]]' ) -ge ${LAPS_SHLDAP_MINIMUM} ;
then
   ferror "${scriptfile}: 4. Insufficient version of shldap found: ${LAPS_SHLDAP_discovered} ${SHLDAP_VERSION}. Need >= ${LAPS_SHLDAP_MINIMUM}. Aborted."
   exit 4
fi

# SET CUSTOM SCRIPT AND VALUES
#setval 1 sendsh sendopts<<EOFSENDSH     # if $1="1" then setvalout="critical-fail" on failure
#/usr/local/share/bgscripts/send.sh -hs  # setvalout maybe be "fail" otherwise
#/usr/share/bgscripts/send.sh -hs        # on success, setvalout="valid-sendsh"
#/usr/local/bin/send.sh -hs
#/usr/bin/mail -s
#EOFSENDSH
#test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. mailer not found. Aborted." && exit 4

# VALIDATE PARAMETERS
# objects before the dash are options, which get filled with the optvals
# to debug flags, use option DEBUG. Variables set in framework: fallopts
validateparams - "$@"

# LEARN EX_DEBUG
test -z "${__debug_set_by_param}" && fisnum "${LAPS_DEBUG}" && debug="${LAPS_DEBUG}"

# CONFIRM TOTAL NUMBER OF FLAGLESSVALS IS CORRECT
#if test ${thiscount} -lt 2;
#then
#   ferror "${scriptfile}: 2. Fewer than 2 flaglessvals. Aborted."
#   exit 2
#fi

# LOAD CONFIG FROM SIMPLECONF
# This section follows a simple hierarchy of precedence, with first being used:
#    1. parameters and flags
#    2. environment
#    3. config file
#    4. default user config: ~/.config/script/script.conf
#    5. default config: /etc/script/script.conf
if test -f "${conffile}";
then
   get_conf "${conffile}"
else
   if test "${conffile}" = "${default_conffile}" || test "${conffile}" = "${defuser_conffile}"; then :; else test -n "${conffile}" && ferror "${scriptfile}: Ignoring conf file which is not found: ${conffile}."; fi
fi
test -f "${defuser_conffile}" && get_conf "${defuser_conffile}"
test -f "${default_conffile}" && get_conf "${default_conffile}"

# CONFIGURE VARIABLES AFTER PARAMETERS
test -z "${LAPS_TMPDIR}" && LAPS_TMPDIR="$( mktemp -d /tmp/laps.XXXXXXXXXX )"
test -z "${LAPS_KRB5CC_TMPFILE}" && LAPS_KRB5CC_TMPFILE="$( TMPDIR="${LAPS_TMPDIR}" mktemp )"
test -z "${LAPS_LDIF_TMPFILE}" && LAPS_LDIF_TMPFILE="$( TMPDIR="${LAPS_TMPDIR}" mktemp )"
test -z "${LAPS_LDAPMODIFY_STATUS_TMPFILE}" && LAPS_LDAPMODIFY_STATUS_TMPFILE="$( TMPDIR="${LAPS_TMPDIR}" mktemp )"
test -z "${LAPS_LDAPSEARCH_STATUS_TMPFILE}" && LAPS_LDAPSEARCH_STATUS_TMPFILE="$( TMPDIR="${LAPS_TMPDIR}" mktemp )"
test -z "${LAPS_PASSWORD_STATUS_TMPFILE}" && LAPS_PASSWORD_STATUS_TMPFILE="$( TMPDIR="${LAPS_TMPDIR}" mktemp )"
#test -z "${LAPS_TMPFILE1}" && LAPS_TMPFILE1="$( TMPDIR="${LAPS_TMPDIR}" mktemp )"
define_if_new LAPS_KINIT_HOST_SCRIPT "/usr/share/bgscripts/work/kinit-host.sh"
define_if_new LAPS_KINIT_HOST_SCRIPT_OPTS ""
define_if_new LAPS_KINIT_HOST_SCRIPT_DEFAULT "/usr/share/bgscripts/work/kinit-host.sh"
define_if_new LAPS_KLIST_BIN "/usr/bin/klist"
define_if_new LAPS_KINIT_BIN "/usr/bin/kinit"
define_if_new LAPS_LDAPSEARCH_BIN "/usr/bin/ldapsearch"
define_if_new LAPS_LDAPSEARCH_FLAGS "-LLL -O maxssf=0 -o ldif-wrap=300 -Y gssapi"
define_if_new LAPS_HOST "$( hostname -s )"
define_if_new LAPS_LDAPSEARCH_FILTER "(cn=${LAPS_HOST}*)"
define_if_new LAPS_LDAPSEARCH_UNIQUE_ID "dn"
define_if_new LAPS_ATTRIB_PW "ms-Mcs-AdmPwd"
define_if_new LAPS_ATTRIB_TIME "ms-Mcs-AdmPwdExpirationTime"
define_if_new LAPS_LDAPMODIFY_BIN "/usr/bin/ldapmodify"
define_if_new LAPS_LDAPMODIFY_FLAGS "-O maxssf=0 -Q -o ldif-wrap=300 -Y gssapi -f"
define_if_new LAPS_LDAPCONF "/etc/laps/lapsldap.conf"
define_if_new LAPS_DATETIME_PY "/usr/share/laps/dependencies/datetime.py"
define_if_new LAPS_THRESHOLD "5 days"
define_if_new LAPS_TIMELIMIT "45 days"
define_if_new LAPS_PWGEN_SCRIPT "/usr/bin/pwmake"
define_if_new LAPS_PWGEN_FLAGS "130"
define_if_new LAPS_USER "floot"
define_if_new LAPS_PASSWD_BIN "$(which passwd)"
define_if_new LAPS_LOG_BIN "$(which logger)"
define_if_new LAPS_LOG_FLAGS "-t laps -i -p authpriv.notice"
define_if_new LAPS_LOG_MSG "LAPS has updated the password for user ${LAPS_USER}"
define_if_new LAPS_TEST 0
define_if_new LAPS_FORCE 0
define_if_new LAPS_KERBEROS_USER "machine"
define_if_new LAPS_ACTION "update"
define_if_new LAPS_INTERACTIVE 0

## REACT TO BEING A CRONJOB
#if test ${is_cronjob} -eq 1;
#then
#   :
#else
#   :
#fi

# SET TRAPS
#trap "CTRLC" 2
#trap "CTRLZ" 18
# do NOT catch SIGCHLD (17) because dash will actually exit then (bash seems to
# ignore our attempt to catch it)
trap '__ec=$? ; clean_laps ; trap "" 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18 19 20 ; exit ${__ec} ;' 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18 19 20

# DEBUG SIMPLECONF
debuglev 5 && {
   ferror "Using values"
   # used values: EX_(OPT1|OPT2|VERBOSE)
   set | grep -iE "^LAPS_" 1>&2
}

# MAIN LOOP
#{
   echo "action ${LAPS_ACTION}" | debuglevoutput 4
   case "${LAPS_ACTION}" in
      read)
         read_workflow
         ;;
      update|*)
         main_workflow
         ;;
   esac

#} | tee -a ${logfile}

# EMAIL LOGFILE
#${sendsh} ${sendopts} "${server} ${scriptfile} out" ${logfile} ${interestedparties}
